package lp2g42.biblioteca;

import java.io.Serial;
import java.io.Serializable;
import java.util.GregorianCalendar;

public class Pessoa implements Serializable {
    @Serial
    private static final long serialVersionUID = -8955043982930517628L;

    private String nome;
    private GregorianCalendar dataNasc;

    public Pessoa(String nome, GregorianCalendar dataNasc) {
        this.nome = nome;
        this.dataNasc = dataNasc;
    }

    public String getNome() {
        return nome;
    }

    public GregorianCalendar getDataNasc() {
        return dataNasc;
    }
}
