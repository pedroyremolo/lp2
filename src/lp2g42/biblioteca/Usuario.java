package lp2g42.biblioteca;

import java.io.Serial;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Usuario extends Pessoa implements Comparable<Usuario> {
    @Serial
    private static final long serialVersionUID = -71705906014609932L;

    private String endereco;
    private int codigo;
    private ArrayList<Emprestimo> historico;

    public Usuario(String nome, GregorianCalendar dataNasc, String endereco, int codigo) {
        super(nome, dataNasc);
        this.endereco = endereco;
        this.codigo = codigo;
        this.historico = new ArrayList<>();
    }

    public String getEndereco() {
        return endereco;
    }

    public int getCodigo() {
        return codigo;
    }

    public ArrayList<Emprestimo> getHistorico() {
        return historico;
    }

    public void addLivroHist(GregorianCalendar dataEmprestimo, String codLivro) {
        var emprestimo = new Emprestimo(dataEmprestimo, null, codLivro);
        historico.add(emprestimo);
    }

    public ArrayList<Emprestimo> getEmprestimosEmAberto() {
        var emprestimos = new ArrayList<Emprestimo>();
        this.historico.stream()
                .filter((emprestimo -> emprestimo.getDataDevolucao() == null))
                .forEach(emprestimos::add);
        return emprestimos;
    }

    public void atualizaHistorico(Emprestimo atual, Emprestimo novo) {
        var idxAtual = this.historico.indexOf(atual);
        this.historico.set(idxAtual, novo);
    }

    @Override
    public int compareTo(Usuario usuario) {
        return this.getNome().compareTo(usuario.getNome());
    }

    @Override
    public String toString() {
        return "Usuario{\n" +
                "\tcodigo=" + codigo + '\n' +
                "\tnome='" + this.getNome() + "'\n" +
                "\tendereco='" + endereco + "'\n" +
                "\thistorico=" + historico + '\n' +
                "}\n";
    }
}
