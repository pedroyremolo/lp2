package lp2g42.biblioteca;

import java.io.Serial;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class EmprestadoPara implements Serializable {
    @Serial
    private static final long serialVersionUID = -6480801647693975168L;

    private GregorianCalendar dataEmprestimo;
    private GregorianCalendar dataDevolucao;
    private int codUsuario;

    public EmprestadoPara(GregorianCalendar dataEmprestimo, GregorianCalendar dataDevolucao, int codUsuario) {
        this.dataEmprestimo = dataEmprestimo;
        this.dataDevolucao = dataDevolucao;
        this.codUsuario = codUsuario;
    }

    public GregorianCalendar getDataEmprestimo() {
        return dataEmprestimo;
    }

    public GregorianCalendar getDataDevolucao() {
        return dataDevolucao;
    }

    public int getCodUsuario() {
        return codUsuario;
    }

    @Override
    public String toString() {
        var formatter = new SimpleDateFormat("dd/MM/yyyy");
        return "\nEmprestadoPara{\n" +
                "\tcodUsuario=" + codUsuario + "\n" +
                "\tdataEmprestimo=" + formatter.format(dataEmprestimo.getTime()) + "\n" +
                "\tdataDevolucao=" + formatter.format(dataDevolucao.getTime()) + "\n" +
                '}';
    }
}
