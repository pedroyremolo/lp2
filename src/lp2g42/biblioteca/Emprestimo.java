package lp2g42.biblioteca;

import java.io.Serial;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Emprestimo implements Serializable {
    @Serial
    private static final long serialVersionUID = -2031281625833715497L;

    private GregorianCalendar dataEmprestimo;
    private GregorianCalendar dataDevolucao;
    private String codLivro;

    public Emprestimo(GregorianCalendar dataEmprestimo, GregorianCalendar dataDevolucao, String codLivro) {
        this.dataEmprestimo = dataEmprestimo;
        this.dataDevolucao = dataDevolucao;
        this.codLivro = codLivro;
    }

    public GregorianCalendar getDataEmprestimo() {
        return dataEmprestimo;
    }

    public GregorianCalendar getDataDevolucao() {
        return dataDevolucao;
    }

    public String getCodLivro() {
        return codLivro;
    }

    @Override
    public String toString() {
        var formatter = new SimpleDateFormat("dd/MM/yyyy");
        var dataDevStr = this.dataDevolucao == null ? "não há": formatter.format(this.dataDevolucao.getTime());
        return "\nEmprestimo{\n" +
                "\tdataEmprestimo=" + formatter.format(dataEmprestimo.getTime()) + "\n" +
                "\tdataDevolucao=" + dataDevStr + "\n" +
                "\tcodLivro='" + codLivro + "'\n" +
                "}";
    }
}
