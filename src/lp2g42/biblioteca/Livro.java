package lp2g42.biblioteca;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Livro implements Comparable<Livro>, Serializable {
    @Serial
    private static final long serialVersionUID = 1194629917165988002L;

    private String codigo;
    private String tituloLivro;
    private String categoria;
    private int quantidade;
    private int emprestados;
    private ArrayList<EmprestadoPara> historico;

    public Livro(String codigo, String tituloLivro, String categoria, int quantidade, int emprestados) {
        this.codigo = codigo;
        this.tituloLivro = tituloLivro;
        this.categoria = categoria;
        this.quantidade = quantidade;
        this.emprestados = emprestados;
        this.historico = new ArrayList<>();
    }

    public Livro(String tituloLivro) {
        this.tituloLivro = tituloLivro;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getTituloLivro() {
        return tituloLivro;
    }

    public String getCategoria() {
        return categoria;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public int getEmprestados() {
        return emprestados;
    }

    public void empresta() throws CopiaNaoDisponivelEx {
        if (quantidade == emprestados) {
            throw new CopiaNaoDisponivelEx(this.codigo);
        }
        emprestados++;
    }

    public void devolve() throws NenhumaCopiaEmprestadaEx {
        if (emprestados == 0) {
            throw new NenhumaCopiaEmprestadaEx(this.codigo);
        }
        emprestados--;
    }

    public void addUsuarioHist(GregorianCalendar dataEmprestimo, GregorianCalendar dataDevolucao, int codUsuario) {
        var emprestadoPara = new EmprestadoPara(dataEmprestimo, dataDevolucao, codUsuario);
        historico.add(emprestadoPara);
    }

    @Override
    public int compareTo(Livro livro) {
        return this.tituloLivro.compareTo(livro.tituloLivro);
    }

    @Override
    public String toString() {
        return "Livro{\n" +
                "\tcodigo='" + codigo + "'\n" +
                "\ttitulo='" + tituloLivro + "'\n" +
                "\tcategoria='" + categoria + "'\n" +
                "\tquantidade=" + quantidade + '\n' +
                "\temprestados=" + emprestados + '\n' +
                "\thistorico=" + historico + '\n' +
                "}\n";
    }

    public void atualizaDataDevolucaoHistorico(GregorianCalendar dataEmprestimo, int codUsuario) {
        for (int i = this.historico.size()-1; i >= 0; i--) {
            var emprestadoPara = this.historico.get(i);
            if (emprestadoPara != null && emprestadoPara.getDataEmprestimo().equals(dataEmprestimo)
                    && emprestadoPara.getCodUsuario()==codUsuario) {
                this.historico.set(
                        i,
                        new EmprestadoPara(
                                emprestadoPara.getDataEmprestimo(),
                                new GregorianCalendar(),
                                codUsuario
                        )
                );
            }
        }
    }
}
