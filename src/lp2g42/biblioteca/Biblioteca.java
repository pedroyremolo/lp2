package lp2g42.biblioteca;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Map.Entry.comparingByValue;

import static java.util.stream.Collectors.*;
import static java.util.Map.Entry.*;

public class Biblioteca {
    private static Hashtable<Integer, Usuario> armazenamentoUsuarios;
    private static Hashtable<String, Livro> armazenamentoLivros;

    public Biblioteca() {
        armazenamentoUsuarios = new Hashtable<>();
        armazenamentoLivros = new Hashtable<>();
    }

    public Biblioteca(String pathArqUsuarios, String pathArqLivros) throws IOException {
        try {
            this.leArquivo(pathArqUsuarios);
        } catch (EOFException | ClassNotFoundException e) {
            armazenamentoUsuarios = new Hashtable<>();
        }

        try {
            this.leArquivo(pathArqLivros);
        } catch (EOFException | ClassNotFoundException e) {
            armazenamentoLivros = new Hashtable<>();
        }
    }

    public static Hashtable<Integer, Usuario> getArmazenamentoUsuarios() {
        return armazenamentoUsuarios;
    }

    public static Hashtable<String, Livro> getArmazenamentoLivros() {
        return armazenamentoLivros;
    }

    public void cadastraUsuario(Usuario usuario) {
        armazenamentoUsuarios.put(usuario.getCodigo(), usuario);
    }

    public void cadastraLivro(Livro livro) {
        armazenamentoLivros.put(livro.getCodigo(), livro);
    }

    public void salvaArquivo(Serializable armazenamento, String pathArquivo) throws IOException {
        var fos = new FileOutputStream(pathArquivo);
        var os = new ObjectOutputStream(fos);

        os.writeObject(armazenamento);
        os.close();
    }

    public void leArquivo(String file) throws IOException, ClassNotFoundException {
        var fis = new FileInputStream(file);
        var is = new ObjectInputStream(fis);

        if (file.contains("livros.ht")) {
            @SuppressWarnings("unchecked")
            var al = (Hashtable<String, Livro>) is.readObject();
            armazenamentoLivros = al;
        } else if (file.contains("usuarios.ht")) {
            @SuppressWarnings("unchecked")
            var au = (Hashtable<Integer, Usuario>) is.readObject();
            armazenamentoUsuarios = au;
        }
    }

    public void emprestaLivro(Usuario usuario, Livro livro) throws CopiaNaoDisponivelEx {
        livro.empresta();
        usuario.addLivroHist(new GregorianCalendar(), livro.getCodigo());
    }

    public void devolveLivro(Usuario usuario, Livro livro) throws NenhumaCopiaEmprestadaEx {
        livro.devolve();
    }

    public String imprimeLivros() {
        StringBuilder result = new StringBuilder();
        var livros = armazenamentoLivros.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new)
                );
        for (String key : livros.keySet()) {
            result.append(livros.get(key).toString());
        }
        return result.toString();
    }

    public String imprimeUsuarios() {
        StringBuilder result = new StringBuilder();
        armazenamentoUsuarios.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(item -> result.append(item.getValue()));
        return result.toString();
    }

    public Livro getLivro(String codigo) throws LivroNaoCadastradoEx {
        var livro = armazenamentoLivros.get(codigo);
        if (livro == null) {
            throw new LivroNaoCadastradoEx(codigo);
        }
        return livro;
    }

    public Usuario getUsuario(int codigo) throws UsuarioNaoCadastradoEx {
        var usuario = armazenamentoUsuarios.get(codigo);
        if (usuario == null) {
            throw new UsuarioNaoCadastradoEx(codigo);
        }
        return usuario;
    }
}
