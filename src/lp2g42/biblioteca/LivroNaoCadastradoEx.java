package lp2g42.biblioteca;

public class LivroNaoCadastradoEx extends IllegalArgumentException {
    public LivroNaoCadastradoEx(String codigoLivro) {
        super("O livro de código " + codigoLivro + " não existe em nosso acervo");
    }
}
