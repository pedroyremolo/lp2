package lp2g42.biblioteca;

public class NenhumaCopiaEmprestadaEx extends IllegalArgumentException {
    public NenhumaCopiaEmprestadaEx(String codLivro) {
        super("O livro de código " + codLivro + " não possui nenhuma cópia emprestada");
    }
}
