package lp2g42.biblioteca;

public class CopiaNaoDisponivelEx extends IllegalArgumentException {
    public CopiaNaoDisponivelEx(String codLivro) {
        super("Livro de código" + codLivro + "não possui mais cópias pra empréstimo");
    }
}
