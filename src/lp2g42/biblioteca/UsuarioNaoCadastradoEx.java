package lp2g42.biblioteca;

public class UsuarioNaoCadastradoEx extends IllegalArgumentException {
    public UsuarioNaoCadastradoEx(int codigo) {
        super("Infelizmente não encontramos o usuário de código " + codigo + " em nossa base.");
    }
}
