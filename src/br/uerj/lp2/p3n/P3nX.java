package br.uerj.lp2.p3n;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class P3nX {
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Digite o número de ângulos a serem inseridos:");
        var numDeEntradas = Integer.parseInt(reader.readLine());

        ArrayList<AnguloObj> anguloObjs = new ArrayList<>();
        try {
            pedeAngulos(reader, numDeEntradas, anguloObjs);
        } catch (NumberFormatException e) {
            System.out.println("O valor digitado não é um número, favor verificar e tentar novamente");
            System.out.println("O programa será encerrado");
            reader.close();
            return;
        }

        imprimeResultado(anguloObjs);
        reader.close();
    }

    private static void imprimeResultado(ArrayList<AnguloObj> anguloObjs) {
        System.out.println("=============== Resultado ==============");
        for (AnguloObj angObj : anguloObjs) {
            System.out.println(angObj);
        }
    }

    private static void pedeAngulos(BufferedReader reader, int numDeEntradas, ArrayList<AnguloObj> anguloObjs) throws IOException {
        while (numDeEntradas > 0) {
            numDeEntradas--;
            System.out.println("Insira a medida do ângulo desejado");
            var angGrau = Double.parseDouble(reader.readLine());
            anguloObjs.add(new AnguloObj(angGrau));
        }
    }
}
