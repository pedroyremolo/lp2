package br.uerj.lp2.p3n;

public class AnguloObj {
    private double arcoRad;

    public AnguloObj(double anguloGrau) {
        arcoRad = Math.toRadians(anguloGrau);
    }

    public double converteAngulo() {
        return arcoRad;
    }

    public double funcaoSeno() {
        return Math.sin(arcoRad);
    }

    public double funcaoCoseno() {
        return Math.cos(arcoRad);
    }

    public double funcaoTangente() {
        return Math.tan(arcoRad);
    }

    public double funcaoCotangente() {
        return 1.0/Math.tan(arcoRad);
    }

    @Override
    public String toString() {
        var str = "";

        str += String.format("Arco: %.2f\n", this.converteAngulo());
        str += String.format("Seno: %.2f\n", this.funcaoSeno());
        str += String.format("Cosseno: %.2f\n", this.funcaoCoseno());
        str += String.format("Tangente: %.2f\n", this.funcaoTangente());
        str += String.format("Cotangente: %.2f\n", this.funcaoCotangente());
        return str;
    }
}
