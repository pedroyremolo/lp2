package br.uerj.lp2.p2n;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P2nX {
    public static void main(String[] args) throws IOException {
        double anguloGraus;
        var reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            if (args.length != 1) {
                anguloGraus = getAnguloGraus(reader);
            } else {
                anguloGraus = Double.parseDouble(args[0]);
            }

            while (true) {
                calcula(anguloGraus);
                anguloGraus = getAnguloGraus(reader);
            }
        } catch (NumberFormatException e) {
            System.out.println("O valor digitado não corresponde a um ângulo válido,"
                    + " tente novamente informando um ângulo em graus");
        }
        reader.close();
    }

    private static double getAnguloGraus(BufferedReader reader) throws IOException {
        double anguloGraus;
        System.out.println("Informe um ângulo em graus, ou aperte ENTER para encerrar o programa:");
        var line = reader.readLine();
        if (line.compareTo("") == 0) {
            System.out.println("Encerrando o programa");
            reader.close();
            System.exit(-1);
        }
        anguloGraus = Double.parseDouble(line);
        return anguloGraus;
    }

    public static void calcula(double anguloGraus) {
        System.out.printf("Angulo: %.2f\n", anguloGraus);
        var anguloRad = Angulo.cvtAngulo(anguloGraus);

        System.out.printf("Em radianos: %.2f\n", anguloRad);
        System.out.printf("Seno: %.2f\n", Angulo.fSeno(anguloRad));
        System.out.printf("Cosseno: %.2f\n", Angulo.fCos(anguloRad));
        System.out.printf("Tangente: %.2f\n", Angulo.fTangente(anguloRad));
        System.out.printf("Cotangente: %.2f\n", Angulo.fCotangente(anguloRad));
    }
}
