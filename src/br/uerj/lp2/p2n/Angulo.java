package br.uerj.lp2.p2n;

public class Angulo {
    public static double cvtAngulo(double graus) {
        return Math.toRadians(graus);
    }

    public static double fSeno(double radianos) {
        return Math.sin(radianos);
    }

    public static double fCos(double radianos) {
        return Math.cos(radianos);
    }

    public static double fTangente(double radianos) {
        return Math.tan(radianos);
    }

    public static double fCotangente(double radianos) {
        return 1.0/Math.tan(radianos);
    }
}
