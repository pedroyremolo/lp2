package br.uerj.lp2.p4n;

public class FuncCntrd extends Funcionario{
    private int numDependentes;
    private float salarioFamilia;
    private final float valorPorDep = 9.58f;
    private final float aliquotaIR = 0.15f;

    public FuncCntrd(String codigoEmpregado, String nome, float salario, int numDependentes) {
        super(codigoEmpregado, nome, salario);
        this.numDependentes = numDependentes;
    }

    public float calculaSalario() {
        if (numDependentes > 0) {

        }
        return super.calculaSalario(this.aliquotaIR);
    }
}
