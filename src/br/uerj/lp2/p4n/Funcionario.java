package br.uerj.lp2.p4n;

public class Funcionario {
    private String codigoEmpregado;
    private String nome;
    private float salario;
    private float salarioLiquido;

    public Funcionario(String codigoEmpregado, String nome, float salario) {
        this.codigoEmpregado = codigoEmpregado;
        this.nome = nome;
        this.salario = salario;
        this.salarioLiquido = salario;
    }

    public float calculaSalario(float desconto) {
        this.salarioLiquido = this.salario - this.salario * desconto;
        return this.salarioLiquido;
    }

    public String getCodigoEmpregado() {
        return codigoEmpregado;
    }

    public String getNome() {
        return nome;
    }

    public float getSalario() {
        return salario;
    }

    public float getSalarioLiquido() {
        return salarioLiquido;
    }

    @Override
    public String toString() {
        return  "Código: " + codigoEmpregado + '\n' +
                "Nome: " + nome + '\n' +
                "Salário-base: " + salario + '\n';
    }
}
