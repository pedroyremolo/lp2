package br.uerj.lp2.ex1n;

import java.io.*;

public class Main {
    public static void main(String[] args) {

        var reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Bem vindo ao contador de letras");
        var phrase = getPhrase(reader);
        while (!(phrase.compareTo("\n") == 0 || phrase.isEmpty())) {
            var letterCounter = new LetterCounter();
            letterCounter.incLetterCountFrom(phrase);
            System.out.println(letterCounter);
            phrase = getPhrase(reader);
        }

        System.out.println("Programa encerrado com sucesso");
    }

    private static String getPhrase(BufferedReader reader) {
        String phrase;
        System.out.println("Insira uma frase para ser levada em consideração,"
                + "caso deseje encerrar o programa apenas aperte ENTER");
        try {
            phrase = reader.readLine();
        } catch (IOException e) {
            return "";
        }
        return phrase;
    }
}
