package br.uerj.lp2.ex1n;

import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

public class LetterCounter {
    private SortedMap<Character, Integer> counterMap;

    public LetterCounter() {
        Comparator<Character> characterComparator = new CharacterComparator();
        counterMap = new TreeMap<>(characterComparator);
    }

    private int getLetterCount(char letter) {
        letter = Character.toLowerCase(letter);
        return counterMap.get(letter) != null ? counterMap.get(letter) : 0;
    }

    private void incLetterCount(char letter) {
        if (Character.isLetter(letter)) {
            letter = Character.toLowerCase(letter);
            var currentValue = this.getLetterCount(letter);
            counterMap.put(letter, currentValue + 1);
        }
    }

    public void incLetterCountFrom(String phrase) {
        var chars = phrase.toCharArray();
        for (char ch : chars) {
            this.incLetterCount(ch);
        }
    }

    @Override
    public String toString() {
        return String.format("Contagem de letras: %s", this.counterMap);
    }
}