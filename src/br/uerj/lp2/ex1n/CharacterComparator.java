package br.uerj.lp2.ex1n;

import java.util.Comparator;

public class CharacterComparator implements Comparator<Character> {

    @Override
    public int compare(Character character, Character t1) {
        return character.compareTo(t1);
    }
}