package br.uerj.lp2.p6n;

public abstract class PessoaIMC extends Pessoa {
    protected double peso, altura;

    public PessoaIMC(String nome, String dataNascimento, double peso, double altura) {
        super(nome, dataNascimento);
        this.peso = peso;
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public double getAltura() {
        return altura;
    }

    public double calculaIMC(double altura, double peso) {
        return peso/(Math.pow(altura, 2));
    }

    public abstract String resultIMC();

    @Override
    public String toString() {
        return super.toString() +
                "Peso: " + String.format("%.2f", this.peso) + "\n" +
                "Altura: " + String.format("%.2f", this.altura) + "\n";
    }
}
