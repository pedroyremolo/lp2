package br.uerj.lp2.p6n;

public class Pessoa {
    protected String nome;
    protected String dataNascimento;

    public Pessoa(String nome, String dataNascimento) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
    }

    @Override
    public String toString() {
        return "\nNome: " + this.nome + "\n" +
                "Data de Nascimento: " + this.dataNascimento + "\n";
    }
}
