package br.uerj.lp2.p6n;

public class Homem extends PessoaIMC{
    public Homem(String nome, String dataNascimento, double peso, double altura) {
        super(nome, dataNascimento, peso, altura);
    }

    @Override
    public String resultIMC() {
        var builder = new StringBuilder();
        var imc = this.calculaIMC(this.altura, this.peso);
        builder.append(String.format("%.2f", imc));
        if (imc > 26.4) {
            builder.append(" Acima do peso ideal");
        } else if (imc <= 26.4 && imc >= 20.7) {
            builder.append(" Peso ideal");
        } else {
            builder.append(" Abaixo do peso ideal");
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return super.toString() +
                "IMC: " + this.resultIMC() + '\n';
    }
}
