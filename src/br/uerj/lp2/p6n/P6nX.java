package br.uerj.lp2.p6n;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P6nX {
    public static void main(String[] args) {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        var listaOrdenavel = getMinhaListaOrdenavel();
        System.out.println("Bem vindo ao ordenador de pessoas!");
        try {
            var sair = false;
            while (!sair) {
                System.out.println("1. Imprimir Lista\n2. Sair");
                System.out.println("Digite sua opção: ");
                switch (reader.readLine()) {
                    case "1" -> {
                        System.out.println("Escolha seu método de ordenação");
                        System.out.println("1. Alfabética (A-Z)");
                        System.out.println("2. Alfabética (Z-A)");
                        System.out.println("3. Peso Crescente");
                        System.out.println("4. Peso Descrescente");
                        System.out.println("5. Altura Crescente");
                        System.out.println("6. IMC Crescente");
                        System.out.println("7. Gênero");
                        System.out.println("Digite: ");
                        listaOrdenavel.ordena(Integer.parseInt(reader.readLine()));
                        System.out.println(listaOrdenavel);
                    }
                    case "2" -> {
                        sair = true;
                    }
                    default -> System.out.println("Opção não encontrada, tente novamente.");
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("Opção informada não corresponde a um número inteiro entre 1 e 7. Tente novamente");
        } catch (IOException e) {
            System.out.println("Erro ao realizar leitura de dados");
        }
    }

    public static MinhaListaOrdenavel getMinhaListaOrdenavel() {
        var listaOrdenavel = new MinhaListaOrdenavel();

        listaOrdenavel.add(new Mulher("Maria", "12/01/1968", 54, 1.72));
        listaOrdenavel.add(new Mulher("Ana", "12/08/1998", 48, 1.65));
        listaOrdenavel.add(new Mulher("Mariane", "17/01/1990", 43, 1.63));
        listaOrdenavel.add(new Mulher("Isabella", "12/06/1987", 62, 1.78));
        listaOrdenavel.add(new Mulher("Larissa", "12/01/1984", 60, 1.73));
        listaOrdenavel.add(new Mulher("Zélia", "14/05/1989", 62, 1.63));
        listaOrdenavel.add(new Homem("Carlos", "14/05/1989", 92, 1.85));
        listaOrdenavel.add(new Homem("Pedro", "14/05/1989", 98, 1.83));
        listaOrdenavel.add(new Homem("Samuel", "14/05/1989", 75, 1.75));
        listaOrdenavel.add(new Homem("Augusto", "14/05/1989", 80, 1.71));

        return listaOrdenavel;
    }
}
