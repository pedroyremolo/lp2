package br.uerj.lp2.p1n;

public class P1nX {
    public static void main(String[] args) {
        double area;
        String figura;
        try {
            switch (args.length) {
                case 1:
                    var raio = Double.parseDouble(args[0]);
                    area = calcula(raio);
                    figura = "círculo";
                    break;
                case 2:
                    var b = Double.parseDouble(args[0]);
                    var a = Double.parseDouble(args[1]);
                    area = calcula(b, a);
                    figura = "retângulo";
                    break;
                case 3:
                    var l1 = Double.parseDouble(args[0]);
                    var l2 = Double.parseDouble(args[1]);
                    var l3 = Double.parseDouble(args[2]);
                    area = calcula(l1, l2, l3);
                    figura = "triângulo";
                    break;
                default:
                    System.out.printf("Número de argumentos %d é invalido," +
                            " favor inserir de 1 a 3 números de dupla precisão\n", args.length);
                    return;
            }
        } catch (NumberFormatException e) {
            System.out.println("Argumento passado não é um número válido, favor passar números de dupla precisão positivos");
            return;
        }

        if (area==0) {
            System.out.printf("Dados de formação do %s inválidos, portanto não há área\n", figura);
            return;
        }
        System.out.printf("A área do %s é %.2f unidades de área\n", figura, area);
    }

    public static double calcula(double r) {
        return Math.PI * Math.pow(r, 2);
    }

    public static double calcula(double b, double a) {
        return b * a;
    }

    public static double calcula(double l1, double l2, double l3) {
        if ((l1 + l2 > l3) && (l1 + l3 > l2) && (l1 + l2 > l3)) {
            // Fórmula de Heron
            var p = (l1 + l2 + l3) / 2;
            return Math.sqrt(p * (p - l1) * (p - l2) * (p - l3));
        }
        return 0;
    }
}
