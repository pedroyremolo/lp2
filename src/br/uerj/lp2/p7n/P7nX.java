package br.uerj.lp2.p7n;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P7nX {
    public static void main(String[] args) {
        try {
            var reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Bem vindo a gestão de sua biblioteca!");
            System.out.println("Insira o caminho para o arquivo de configurações:");
            var appPropertiesPath = reader.readLine();

            Manutencao manutencao;
            manutencao = getManutencao(reader, appPropertiesPath);
            if (manutencao == null) return;

            var cadastro = new Cadastro(manutencao);
            var emprestimo = new Emprestimo(manutencao);
            var relatorio = new Relatorio(manutencao);

            runModulosFuncionais(reader, cadastro, emprestimo, relatorio);
        } catch (IOException e) {
            System.out.println("Erro ao realizar leitura de entrada");
        }
    }

    private static void runModulosFuncionais(BufferedReader reader, Cadastro cadastro, Emprestimo emprestimo, Relatorio relatorio) throws IOException {
        var encerra = false;
        while (!encerra) {
            System.out.println("Digite:\n(1) Cadastro;\n(2) Empréstimo de Livros;\n(3) Relatório;\n(4) Encerra");
            switch (reader.readLine()) {
                case "1" -> cadastro.run(reader);
                case "2" -> emprestimo.run(reader);
                case "3" -> relatorio.run(reader);
                case "4" -> {
                    System.out.println("Valeu, até a próxima! O programa está sendo encerrado");
                    encerra = true;
                }
                default -> System.out.println("Ops! Comando não encontrado, tente novamente");
            }
        }
    }

    private static Manutencao getManutencao(BufferedReader reader, String appPropertiesPath) throws IOException {
        System.out.println("Deseja criar novos arquivos, ou carregar de existentes. Digite:");
        System.out.println("(1) Novos;\n(2) Carregar Existentes.");

        Manutencao manutencao;
        switch (reader.readLine()) {
            case "1" -> manutencao = Manutencao.criaArquivos(appPropertiesPath);
            case "2" -> manutencao = Manutencao.abreArquivos(appPropertiesPath);
            default -> {
                System.out.println("Opção inválida, tente novamente");
                return null;
            }
        }
        return manutencao;
    }
}
