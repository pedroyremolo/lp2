package br.uerj.lp2.p7n;

import lp2g42.biblioteca.Biblioteca;
import lp2g42.biblioteca.Livro;
import lp2g42.biblioteca.Usuario;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.UUID;

public class Cadastro {
    private final Manutencao manutencao;
    private final Biblioteca biblioteca;

    public Cadastro(Manutencao manutencao) {
        this.manutencao = manutencao;
        this.biblioteca = manutencao.getBiblioteca();
    }

    private void cadastraLivro(BufferedReader reader) {
        System.out.println("Cadastrando livros....");
        while (true) {
            try {
                System.out.println("Digite o número de livros a ser cadastrado, ou 0 para retornar");
                int qtdLivros;
                qtdLivros = Integer.parseInt(reader.readLine());
                if (qtdLivros <= 0) {
                    return;
                }
                insereLivrBibl(reader, qtdLivros);

                System.out.println("Deseja salvar arquivo de livros(S/n)");
                var salva = reader.readLine();
                if (salva.compareTo("S") == 0 || salva.compareTo("") == 0) {
                    this.salvaArqLivros();
                }
            } catch (IOException e) {
                System.out.println("Erro interno ao ler entrada, programa será encerrado. Tente novamente mais tarde");
                return;
            } catch (NumberFormatException e) {
                System.out.println("Não foi informado um número inteiro, tente novamente");
            }
        }
    }

    private void insereLivrBibl(BufferedReader reader, int qtdLivros) throws IOException {
        while (qtdLivros != 0) {
            System.out.println("Digite o título: ");
            var titulo = reader.readLine();
            System.out.println("Digite a categoria: ");
            var categoria = reader.readLine();
            System.out.println("Digite a quantidade de exemplares: ");
            var quantidade = Integer.parseInt(reader.readLine());

            var emprestados = 0;
            var codigo = UUID.randomUUID().toString();

            var livro = new Livro(codigo, titulo, categoria, quantidade, emprestados);
            this.biblioteca.cadastraLivro(livro);

            qtdLivros--;
        }
    }

    private void cadastraUsuario(BufferedReader reader) {
        System.out.println("Cadastrando usuários...");
        while (true) {
            try {
                System.out.println("Digite o número de usuários a ser cadastrado, ou 0 para retornar");
                int qtdUsuarios;
                qtdUsuarios = Integer.parseInt(reader.readLine());
                if (qtdUsuarios <= 0) {
                    return;
                }
                insereUsrBibl(reader, qtdUsuarios);

                System.out.println("Deseja salvar arquivo de usuários(S/n)");
                var salva = reader.readLine();
                if (salva.compareTo("S") == 0 || salva.compareTo("") == 0) {
                    this.salvaArqUsuarios();
                }
            } catch (IOException e) {
                System.out.println("Erro interno ao ler entrada, programa será encerrado. Tente novamente mais tarde");
                return;
            } catch (NumberFormatException e) {
                System.out.println("Não foi informado um número inteiro, tente novamente");
            } catch (ParseException e) {
                System.out.println("Formato de data inválida informada");
            }
        }
    }

    private void insereUsrBibl(BufferedReader reader, int qtdUsuarios) throws IOException, ParseException {
        while (qtdUsuarios != 0) {
            System.out.println("Digite a matrícula: ");
            var matricula = Integer.parseInt(reader.readLine());

            System.out.println("Digite o nome: ");
            var nome = reader.readLine();

            System.out.println("Digite a data de nascimento(Ex:19/03/1994): ");
            var formatter = new SimpleDateFormat("dd/MM/yyyy");
            var dataNascDate = formatter.parse(reader.readLine());
            var dataNasc = new GregorianCalendar();
            dataNasc.setTime(dataNascDate);

            System.out.println("Digite o endereço do usuário: ");
            var endereco = reader.readLine();

            var usuario = new Usuario(nome, dataNasc, endereco, matricula);
            this.biblioteca.cadastraUsuario(usuario);

            qtdUsuarios--;
        }
    }

    private void salvaArqLivros() {
        System.out.println("Salvando arquivo de livros...");
        this.manutencao.salvaArqLivros();
    }

    private void salvaArqUsuarios() {
        System.out.println("Salvando arquivo de usuários...");
        this.manutencao.salvaArqUsuarios();
    }

    public void run(BufferedReader reader) throws IOException {

        System.out.println("Você entrou no Cadastro");
        System.out.println("Digite:\n(1) Cadastrar Livros;\n(2) Cadastrar Usuários;\n(3) Salvar Arquivos");
        switch (reader.readLine()) {
            case "1" -> this.cadastraLivro(reader);
            case "2" -> this.cadastraUsuario(reader);
            case "3" -> {
                System.out.println("Digite:\n(1) Salvar Arquivo de Livros;\n(2) Salvar Arquivo de Usuários");
                switch (reader.readLine()) {
                    case "1" -> this.salvaArqLivros();
                    case "2" -> this.salvaArqUsuarios();
                }
            }
        }
    }
}
