package br.uerj.lp2.p7n;

import lp2g42.biblioteca.Biblioteca;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Manutencao {

    private final Properties appProperties;

    private final File arqLivros;
    private final File arqUsuarios;
    private Biblioteca biblioteca;

    private int diasEmprestimo = 10;
    private int maxLivrosPorUsuario = 3;
    private double multaPorDia = 2.00;

    private Manutencao(String propertiesFilePath) {
        this.appProperties = new Properties();
        System.out.println("Carregando propriedades...");
        this.carregaProperties(propertiesFilePath);

        this.arqUsuarios = new File(this.appProperties.getProperty("manutencao.armazenamento.usuarios", "usuarios.ht"));
        this.arqLivros = new File(this.appProperties.getProperty("manutencao.armazenamento.livros", "livros.ht"));

        try {
            this.diasEmprestimo = Integer.parseInt(this.appProperties.getProperty("emprestimo.biblioteca.diasEmprestimo", "10"));
        } catch (NumberFormatException e) {
            System.out.println("propriedade diasEmprestimo possui formato não numérico. Carregando valor padrão 10");
        }
        try {
            this.maxLivrosPorUsuario = Integer.parseInt(this.appProperties.getProperty("emprestimo.biblioteca.maxLivrosPorUsuario", "3"));
        } catch (NumberFormatException e) {
            System.out.println("propriedade diasEmprestimo ou maxLivrosPorUsuario possuem formato não numérico. Carregando valor padrão 3");
        }
        try {
            this.multaPorDia = Double.parseDouble(this.appProperties.getProperty("emprestimo.biblioteca.multaPorDia", "2.00"));
        } catch (NumberFormatException e) {
            System.out.println("propriedade multaPorDia deve conter valor de dupla precisão(e.g 2.00). Carregando valor padrão 2.00");
        }
        System.out.println(this);
    }

    public static Manutencao criaArquivos(String propertiesFilePath) {
        var manutencao = new Manutencao(propertiesFilePath);
        try {
            if (manutencao.arqUsuarios.createNewFile()) {
                manutencao.arqUsuarios.setReadable(true);
                System.out.println("Arquivo de usuários criado com sucesso em " + manutencao.arqUsuarios.getAbsolutePath());
            } else {
                System.out.println("Arquivo de usuários já existe em " + manutencao.arqUsuarios.getAbsolutePath());
                System.out.println("Caso não queira sobrescreve-lo, encerre o programa");
            }
            if (manutencao.arqLivros.createNewFile()) {
                manutencao.arqLivros.setReadable(true);
                System.out.println("Arquivo de livros criado com sucesso em " + manutencao.arqLivros.getAbsolutePath());
            } else {
                System.out.println("Arquivo de livros já existe em " + manutencao.arqLivros.getAbsolutePath());
                System.out.println("Caso não queira sobrescreve-lo, encerre o programa");
            }
        } catch (IOException e) {
            System.out.println("Um erro ocorreu ao criar os arquivos");
            return null;
        }
        manutencao.biblioteca = new Biblioteca();
        return manutencao;
    }

    public static Manutencao abreArquivos(String propertiesFilePath) {
        var manutencao = new Manutencao(propertiesFilePath);
        if (manutencao.arqLivros.exists()) {
            manutencao.arqLivros.setReadable(true);
        }
        if (manutencao.arqUsuarios.exists()) {
            manutencao.arqUsuarios.setReadable(true);
        }
        try {
            manutencao.biblioteca = new Biblioteca(manutencao.arqUsuarios.getCanonicalPath(), manutencao.arqLivros.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return manutencao;
    }

    private void carregaProperties(String propertiesFilePath) {
        try {
            var in = new FileInputStream(propertiesFilePath);
            this.appProperties.load(in);
            in.close();
        } catch (IOException e) {
            System.out.println("Erro ao ler arquivo de propriedades" + propertiesFilePath +
                    ", verifique-o e tente novamente. Ou siga com as regras padrões.");
        }
    }

    public void salvaArqLivros() {
        if (this.arqLivros.exists() && this.biblioteca != null) {
            if (this.arqLivros.setWritable(true)) {
                try {
                    this.biblioteca.salvaArquivo(Biblioteca.getArmazenamentoLivros(), this.arqLivros.getAbsolutePath());
                } catch (IOException e) {
                    System.out.println("Falha ao salvar arquivos");
                }
            }
            this.arqLivros.setWritable(false);
            return;
        }
        System.out.println("O arquivo" + this.arqLivros.getName() + " ainda não foi criado," +
                " crie-o antes de tentar salvar");
    }

    public void salvaArqUsuarios() {
        if (this.arqUsuarios.exists() && this.biblioteca != null) {
            if (this.arqUsuarios.setWritable(true)) {
                try {
                    this.biblioteca.salvaArquivo(Biblioteca.getArmazenamentoUsuarios(), this.arqUsuarios.getAbsolutePath());
                } catch (IOException e) {
                    System.out.println("Falha ao salvar arquivos");
                }
            }
            this.arqUsuarios.setWritable(false);
            return;
        }
        System.out.println("O arquivo" + this.arqUsuarios.getName() + " ainda não foi criado," +
                " crie-o antes de tentar salvar");
    }

    public int getDiasEmprestimo() {
        return diasEmprestimo;
    }

    public int getMaxLivrosPorUsuario() {
        return maxLivrosPorUsuario;
    }

    public double getMultaPorDia() {
        return multaPorDia;
    }

    public Biblioteca getBiblioteca() {
        return biblioteca;
    }

    @Override
    public String toString() {
        return "Propriedades carregadas:\n" +
                "arquivo de livros=" + arqLivros.getName() + "\n" +
                "arquivo de usuários=" + arqUsuarios + "\n" +
                "dias de emprestimo por livro=" + diasEmprestimo + "\n" +
                "máximo de livros por usuário=" + maxLivrosPorUsuario + "\n" +
                "multa por dia de atraso=" + multaPorDia + "\n";
    }
}
