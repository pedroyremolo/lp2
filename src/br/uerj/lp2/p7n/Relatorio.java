package br.uerj.lp2.p7n;

import lp2g42.biblioteca.Biblioteca;
import lp2g42.biblioteca.LivroNaoCadastradoEx;
import lp2g42.biblioteca.UsuarioNaoCadastradoEx;

import java.io.BufferedReader;
import java.io.IOException;

public class Relatorio {
    private final Biblioteca biblioteca;

    public Relatorio(Manutencao manutencao) {
        this.biblioteca = manutencao.getBiblioteca();
    }

    public void run(BufferedReader reader) throws IOException {
        System.out.println("Você entrou no Relatório");
        System.out.println("Digite para listar:\n(1) Acervo de Livros;\n(2) Base de Usuários;" +
                "\n(3) Detalhes de um Livro;\n(4) Detalhes de um Usuário");
        switch (reader.readLine()) {
            case "1" -> this.listaAcervo();
            case "2" -> this.listaUsuarios();
            case "3" -> {
                System.out.println("Insira o código do livro:");
                var codigo = reader.readLine();
                this.listaLivro(codigo);
            }
            case "4" -> {
                System.out.println("Insira a matrícula do usuário:");
                try {
                    var matricula = Integer.parseInt(reader.readLine());
                    this.listaUsuario(matricula);
                } catch (NumberFormatException e) {
                    System.out.println("Por favor, repita o procedimento informando apenas números" +
                            " para a matrícula");
                }
            }
        }
    }

    private void listaAcervo() {
        System.out.println("Listando acervo de livros...");
        System.out.println(this.biblioteca.imprimeLivros());
    }

    private void listaUsuarios() {
        System.out.println("Listando base de usuários...");
        System.out.println(this.biblioteca.imprimeUsuarios());
    }

    private void listaLivro(String codigo) {
        System.out.printf("Procurando livro de código %s em nosos acervo...\n", codigo);
        try {
            var livro = this.biblioteca.getLivro(codigo);
            System.out.println("Livro encontrado:");
            System.out.println(livro.toString());
        } catch (LivroNaoCadastradoEx ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void listaUsuario(int codigo) {
        System.out.printf("Procurando usuário de código %d em nossa base...\n", codigo);
        try {
            var usuario = this.biblioteca.getUsuario(codigo);
            System.out.println("Usuário encontrado:");
            System.out.println(usuario.toString());
        } catch (UsuarioNaoCadastradoEx ex) {
            System.out.println(ex.getMessage());
        }
    }
}
