package br.uerj.lp2.p7n;

import lp2g42.biblioteca.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Emprestimo {
    private final Biblioteca biblioteca;
    private final Manutencao manutencao;

    private final int diasEmprestimo;
    private final int maxLivrosPorUsuario;
    private final double multaPorDia;

    public Emprestimo(Manutencao manutencao) {
        this.biblioteca = manutencao.getBiblioteca();
        this.manutencao = manutencao;

        this.diasEmprestimo = manutencao.getDiasEmprestimo();
        this.maxLivrosPorUsuario = manutencao.getMaxLivrosPorUsuario();
        this.multaPorDia = manutencao.getMultaPorDia();
    }

    public void run(BufferedReader reader) throws IOException {
        System.out.println("Você entrou no módulo de Empréstimo");
        System.out.println("Digite:\n(1) Exibir acervo de livros;\n(2) Pegar livro;\n(3) Devolver livro");
        switch (reader.readLine()) {
            case "1" -> this.exibeLivros();
            case "2" -> this.empresta(reader);
            case "3" -> this.devolve(reader);
        }
    }

    public void empresta(BufferedReader reader) throws IOException {
        try {
            System.out.println("Insira a matrícula do usuário:");
            var codUsuario = Integer.parseInt(reader.readLine());
            var usuario = this.biblioteca.getUsuario(codUsuario);
            var emprestimosEmAberto = usuario.getEmprestimosEmAberto();
            if (emprestimosEmAberto.size() == this.maxLivrosPorUsuario) {
                System.out.println("O usuário atingiu o limite de empréstimos. Devolva um livro para pegar outro.");
                return;
            }

            System.out.println("Insira o código do livro desejado:");
            var codLivro = reader.readLine();
            var livro = this.biblioteca.getLivro(codLivro);
            if (emprestimosEmAberto.stream()
                    .filter(emprestimo -> emprestimo.getCodLivro().equals(codLivro))
                    .toArray().length > 0) {
                System.out.println("Apenas um exemplar por usuário é permitido, e este usuário já possui um");
                return;
            }

            var hoje = new GregorianCalendar();
            var dataDevolucao = new GregorianCalendar();
            dataDevolucao.add(Calendar.DAY_OF_MONTH, this.diasEmprestimo);

            this.biblioteca.emprestaLivro(usuario, livro);
            livro.addUsuarioHist(hoje, dataDevolucao, usuario.getCodigo());
            System.out.println("Exemplar de " + livro.getTituloLivro() + "(" + livro.getCodigo() + ") " +
                    "emprestado para " + usuario.getNome() + "(" + usuario.getCodigo() + ")");
            var dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Emprestado em: " + dateFormatter.format(hoje.getTime()));
            System.out.println("Devolver até: " + dateFormatter.format(dataDevolucao.getTime()));
        } catch (NumberFormatException e) {
            System.out.println("A matrícula do usuário é composta apenas por números, tente novamente");
        } catch (UsuarioNaoCadastradoEx | LivroNaoCadastradoEx | CopiaNaoDisponivelEx e) {
            System.out.println(e.getMessage());
        }
    }

    public void devolve(BufferedReader reader) throws IOException {
        try {
            System.out.println("Insira a matrícula do usuário que está realizando a devolução:");
            var codUsuario = Integer.parseInt(reader.readLine());
            var usuario = this.biblioteca.getUsuario(codUsuario);
            var emprestimosEmAberto = usuario.getEmprestimosEmAberto();

            System.out.println("Insira o código do livro que será devolvido:");
            var codLivro = reader.readLine();
            var livro = this.biblioteca.getLivro(codLivro);
            lp2g42.biblioteca.Emprestimo emprestimoAtual = null;
            for (lp2g42.biblioteca.Emprestimo e :
                    emprestimosEmAberto) {
                if (e.getCodLivro().equals(codLivro)) {
                    emprestimoAtual = e;
                }
            }
            if (emprestimoAtual == null) {
                System.out.println("O usuário não possui nenhum exemplar desse livro emprestado");
                return;
            }

            this.biblioteca.devolveLivro(usuario, livro);

            var dataEmprestimo = new GregorianCalendar();
            dataEmprestimo.setTime(emprestimoAtual.getDataEmprestimo().getTime());
            var hoje = new GregorianCalendar();
            var dataDevolucaoEstimada = new GregorianCalendar();
            dataDevolucaoEstimada.setTime(emprestimoAtual.getDataEmprestimo().getTime());
            dataDevolucaoEstimada.add(Calendar.DAY_OF_MONTH, this.diasEmprestimo);

            var emprestimoAtualizado = new lp2g42.biblioteca.Emprestimo(dataEmprestimo, hoje, codLivro);

            var diasDeAtraso = ChronoUnit.DAYS.between(dataDevolucaoEstimada.toInstant(), hoje.toInstant());
            if (diasDeAtraso > 0) {
                var charPlural = diasDeAtraso == 1 ? "" : "s";
                System.out.println("O usuário está entregando o livro " + diasDeAtraso + " dia" + charPlural + " atrasado");
                System.out.println("Portando, deve pagar multa de " + this.multaPorDia + " ao dia." +
                        " Totalizando " + diasDeAtraso * this.multaPorDia);
            }

            usuario.atualizaHistorico(emprestimoAtual, emprestimoAtualizado);
            livro.atualizaDataDevolucaoHistorico(dataEmprestimo, codUsuario);

            System.out.println("Exemplar de " + livro.getTituloLivro() + "(" + livro.getCodigo() + ") " +
                    "devolvido por " + usuario.getNome() + "(" + usuario.getCodigo() + ")");
            var dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Emprestado em: " + dateFormatter.format(dataEmprestimo.getTime()));
            System.out.println("Devolvido em: " + dateFormatter.format(hoje.getTime()));
        } catch (NumberFormatException e) {
            System.out.println("A matrícula do usuário é composta apenas por números, tente novamente");
        } catch (UsuarioNaoCadastradoEx | LivroNaoCadastradoEx | NenhumaCopiaEmprestadaEx e) {
            System.out.println(e.getMessage());
        }
    }

    public void exibeLivros() {
        System.out.println("O acervo atual é:");
        System.out.println(this.biblioteca.imprimeLivros());
    }
}
