package br.uerj.lp2.p8n.models.imc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MinhaListaOrdenavel extends ArrayList<PessoaIMC> {
	private static final long serialVersionUID = -8934110528790356489L;
	
	private final int NOME_CRESCENTE = 1;
    private final int NOME_DECRESCENTE = 2;
    private final int PESO_CRESCENTE = 3;
    private final int PESO_DECRESCENTE = 4;
    private final int ALTURA_CRESCENTE = 5;
    private final int IMC_CRESCENTE = 6;
    private final int GENERO = 7;

    public ArrayList<PessoaIMC> ordena(int criterio) {
        switch (criterio) {
            case NOME_CRESCENTE:
            	Collections.sort(this, nomeCrescC);
            	break;
            case NOME_DECRESCENTE:
            	Collections.sort(this, nomeCrescC.reversed());
            	break;
            case PESO_CRESCENTE:
            	Collections.sort(this, pesoCrescC);
            	break;
            case PESO_DECRESCENTE:
            	Collections.sort(this, pesoCrescC.reversed());
            	break;
            case ALTURA_CRESCENTE:
            	Collections.sort(this, alturaCrescC);
            	break;
            case IMC_CRESCENTE:
            	Collections.sort(this, imcC);
            	break;
            case GENERO:
            	Collections.sort(this, generoC);
            	break;
            default:
                System.out.println("Opção inserida inválida. A lista retornada será desordenada(e.g ordem de criação)");
                try{Thread.sleep(3000);} catch (InterruptedException ignored){};
                return this;
        }
        return this;
    }

    @Override
    public boolean add(PessoaIMC p) {
        return super.add(p);
    }

    @Override
    public PessoaIMC get(int index) {
        return super.get(index);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (PessoaIMC p :
                this) {
            builder.append("-------------------------");
            builder.append(p);
            builder.append("-------------------------");
        }
        return builder.toString();
    }

    public Comparator<PessoaIMC> pesoCrescC = new Comparator<PessoaIMC>() {
        @Override
        public int compare(PessoaIMC p1, PessoaIMC p2) {
            double pesoP2 = p2.getPeso();
            double pesoP1 = p1.getPeso();
            return (int) Math.round(pesoP1 - pesoP2);
        }
    };

    public Comparator<PessoaIMC> alturaCrescC = new Comparator<PessoaIMC>() {
        @Override
        public int compare(PessoaIMC p1, PessoaIMC p2) {
            double alturaP2 = p2.getAltura();
            double alturaP1 = p1.getAltura();
            if (alturaP1 > alturaP2) {
                return 1;
            } else if (alturaP2 > alturaP1) {
                return -1;
            }
            return 0;
        }
    };

    public Comparator<PessoaIMC> nomeCrescC = new Comparator<PessoaIMC>() {
        @Override
        public int compare(PessoaIMC p1, PessoaIMC p2) {
            return p1.nome.compareTo(p2.nome);
        }
    };

    public Comparator<PessoaIMC> imcC = new Comparator<PessoaIMC>() {
        @Override
        public int compare(PessoaIMC p1, PessoaIMC p2) {
            double imcP2 = p2.calculaIMC(p2.getAltura(), p2.getPeso());
            double imcP1 = p1.calculaIMC(p1.getAltura(), p1.getPeso());
            if (imcP1 > imcP2) {
                return 1;
            } else if (imcP2 > imcP1) {
                return -1;
            }
            return 0;
        }
    };

    public Comparator<PessoaIMC> generoC = new Comparator<PessoaIMC>() {
        @Override
        public int compare(PessoaIMC p1, PessoaIMC p2) {
            if (p1 instanceof Mulher && p2 instanceof Homem) {
                return -1;
            } else if (p1 instanceof Homem && p2 instanceof Mulher) {
                return 1;
            }
            return 0;
        }
    };
}
