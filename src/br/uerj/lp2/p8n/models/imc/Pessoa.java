package br.uerj.lp2.p8n.models.imc;

public class Pessoa {
	protected String nome;
	protected String dataNascimento;

	public Pessoa(String nome, String dataNascimento) {
		this.nome = nome;
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	@Override
	public String toString() {
		return "\nNome: " + this.nome + "\n" + "Data de Nascimento: " + this.dataNascimento + "\n";
	}
}
