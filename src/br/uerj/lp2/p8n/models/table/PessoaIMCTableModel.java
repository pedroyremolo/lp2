package br.uerj.lp2.p8n.models.table;

import javax.swing.table.AbstractTableModel;

import br.uerj.lp2.p8n.models.imc.MinhaListaOrdenavel;
import br.uerj.lp2.p8n.models.imc.PessoaIMC;

public class PessoaIMCTableModel extends AbstractTableModel {
	private static final long serialVersionUID = -6304054511962811047L;
	
	private static final int COLUMN_NOME = 0;
	private static final int COLUMN_DATA_NASCIMENTO = 1;
	private static final int COLUMN_SEXO = 2;
	private static final int COLUMN_PESO = 3;
	private static final int COLUMN_ALTURA = 4;
	private static final int COLUMN_RESULTADO_IMC = 5;

	private String[] columnNames = { "Nome", "Data de Nascimento", "Sexo", "Peso", "Altura", "Resultado IMC" };
	private MinhaListaOrdenavel listaPessoaIMC;

	public PessoaIMCTableModel(MinhaListaOrdenavel listaPessoaIMC) {
		this.listaPessoaIMC = listaPessoaIMC;
	}

	@Override
	public int getRowCount() {
		return this.listaPessoaIMC.size();
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}
	
    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (this.listaPessoaIMC.isEmpty()) {
			return Object.class;
		}
		return getValueAt(0, columnIndex).getClass();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		PessoaIMC pessoaIMC = this.listaPessoaIMC.get(rowIndex);
		Object retorno = null;

		switch (columnIndex) {
		case COLUMN_NOME:
			retorno = pessoaIMC.getNome();
			break;
		case COLUMN_DATA_NASCIMENTO:
			retorno = pessoaIMC.getDataNascimento();
			break;
		case COLUMN_SEXO:
			retorno = pessoaIMC.getClass().getCanonicalName()=="models.imc.Mulher"?"Feminino":"Masculino";
			break;
		case COLUMN_PESO:
			retorno = pessoaIMC.getPeso();
			break;
		case COLUMN_ALTURA:
			retorno = pessoaIMC.getAltura();
			break;
		case COLUMN_RESULTADO_IMC:
			retorno = pessoaIMC.resultIMC();
		}
		return retorno;
	}
}
