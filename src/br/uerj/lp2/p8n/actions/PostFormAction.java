package br.uerj.lp2.p8n.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.uerj.lp2.p8n.models.imc.Homem;
import br.uerj.lp2.p8n.models.imc.MinhaListaOrdenavel;
import br.uerj.lp2.p8n.models.imc.Mulher;
import br.uerj.lp2.p8n.models.imc.PessoaIMC;
import br.uerj.lp2.p8n.models.table.PessoaIMCTableModel;

public class PostFormAction extends CleanFormAction implements ActionListener {

	private JTextField nomeField;
	private JTextField dataNascimentoField;
	private JComboBox<String> sexoCombo;
	private JTextField alturaField;
	private JTextField pesoField;
	private MinhaListaOrdenavel listaOrdenavel;
	private PessoaIMCTableModel tableModel;

	public PostFormAction(JTextField nomeField, JTextField dataNascimentoField, JComboBox<String> sexoCombo,
			JTextField alturaField, JTextField pesoField, MinhaListaOrdenavel listaOrdenavel, PessoaIMCTableModel tableModel) {
		super(nomeField, dataNascimentoField, alturaField, pesoField);
		this.nomeField = nomeField;
		this.dataNascimentoField = dataNascimentoField;
		this.alturaField = alturaField;
		this.pesoField = pesoField;
		this.sexoCombo = sexoCombo;
		this.listaOrdenavel = listaOrdenavel;
		this.tableModel = tableModel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Add form validation
		String nome = this.nomeField.getText();
		if (nome.isEmpty()) {
			this.imprimeFormDialogErr("O campo nome n�o pode estar vazio");
			return;
		}
		String dataNascimento = this.dataNascimentoField.getText();
		if (dataNascimento.length()!= 10) {
			this.imprimeFormDialogErr("O campo data nascimento deve seguir o formato dd/MM/yyyy (e.g 19/03/1994)");
			return;
		}
		String sexo = (String) this.sexoCombo.getSelectedItem();
		double altura;
		try {
			altura = Double.parseDouble(this.alturaField.getText()); // TODO handle exception
		} catch (NumberFormatException e1) {
			this.imprimeFormDialogErr("A altura digitada deve ser um n�mero real de dupla precis�o (e.g 1.94)");
			return;
		}
		if (altura <= 0) {
			this.imprimeFormDialogErr("A altura digitada deve ser um n�mero real positivo de dupla precis�o (e.g 1.94)");
			return;
		}
		double peso;
		try {
			peso = Double.parseDouble(this.pesoField.getText()); // TODO handle exception
		} catch (NumberFormatException e2) {
			this.imprimeFormDialogErr("O peso digitado deve ser um n�mero real de dupla precis�o (e.g 92.00)");
			return;
		}
		if (peso <= 0) {
			this.imprimeFormDialogErr("O peso digitado deve ser um n�mero real positivo de dupla precis�o (e.g 92.00)");
		}

		PessoaIMC p = sexo == "Feminino" ? new Mulher(nome, dataNascimento, peso, altura)
				: new Homem(nome, dataNascimento, peso, altura);

		this.listaOrdenavel.add(p);
		this.tableModel.fireTableStructureChanged();
		this.cleanForm();
	}
	
	private void imprimeFormDialogErr(String mensagem) {
		JOptionPane.showMessageDialog(null, mensagem, "Erro, verifique seu formul�rio", JOptionPane.ERROR_MESSAGE);
	}
}
