package br.uerj.lp2.p8n.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

public class CleanFormAction implements ActionListener {

	protected JTextField nomeField;
	protected JTextField dataNascimentoField;
	protected JTextField alturaField;
	protected JTextField pesoField;

	public CleanFormAction(JTextField nomeField, JTextField dataNascimentoField, JTextField alturaField,
			JTextField pesoField) {
		this.nomeField = nomeField;
		this.dataNascimentoField = dataNascimentoField;
		this.alturaField = alturaField;
		this.pesoField = pesoField;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.cleanForm();
	}

	protected void cleanForm() {
		this.nomeField.setText(null);
		this.dataNascimentoField.setText(null);
		this.alturaField.setText(null);
		this.pesoField.setText(null);
	}

}
