package br.uerj.lp2.p8n;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import br.uerj.lp2.p8n.actions.CleanFormAction;
import br.uerj.lp2.p8n.actions.PostFormAction;
import br.uerj.lp2.p8n.models.imc.Homem;
import br.uerj.lp2.p8n.models.imc.MinhaListaOrdenavel;
import br.uerj.lp2.p8n.models.imc.Mulher;
import br.uerj.lp2.p8n.models.table.PessoaIMCTableModel;

public class IMCApplet extends JApplet {
	private static final long serialVersionUID = -2710577379552361774L;

	private JTextField nomeTextField;
	private JTextField dataNascimentoTextField;
	private JTable table;
	private JComboBox<String> comboBox;
	private JTextField alturaTextField;
	private JTextField pesoTextField;

	/**
	 * Create the applet.
	 */
	public IMCApplet() {
		getContentPane().setSize(new Dimension(1000, 800));
		getContentPane().setMinimumSize(new Dimension(1200, 800));
		BorderLayout borderLayout = (BorderLayout) getContentPane().getLayout();
		borderLayout.setVgap(10);
		borderLayout.setHgap(10);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20 };
		gbl_panel.columnWeights = new double[] { 1.0 };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel nomeLabel = new JLabel("Nome");
		GridBagConstraints gbc_nomeLabel = new GridBagConstraints();
		gbc_nomeLabel.insets = new Insets(0, 0, 5, 0);
		gbc_nomeLabel.gridx = 0;
		gbc_nomeLabel.gridy = 0;
		panel.add(nomeLabel, gbc_nomeLabel);

		nomeTextField = new JTextField();
		nomeTextField.setToolTipText("Digite seu nome");
		nomeLabel.setLabelFor(nomeTextField);
		GridBagConstraints gbc_nomeTextField = new GridBagConstraints();
		gbc_nomeTextField.insets = new Insets(0, 0, 5, 0);
		gbc_nomeTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_nomeTextField.gridx = 0;
		gbc_nomeTextField.gridy = 1;
		panel.add(nomeTextField, gbc_nomeTextField);
		nomeTextField.setColumns(10);

		JLabel dataNascimentoLabel = new JLabel("Data de Nascimento");
		dataNascimentoLabel.setLabelFor(dataNascimentoLabel);
		GridBagConstraints gbc_dataNascimentoLabel = new GridBagConstraints();
		gbc_dataNascimentoLabel.insets = new Insets(0, 0, 5, 0);
		gbc_dataNascimentoLabel.gridx = 0;
		gbc_dataNascimentoLabel.gridy = 2;
		panel.add(dataNascimentoLabel, gbc_dataNascimentoLabel);

		dataNascimentoTextField = new JTextField();
		dataNascimentoTextField.setToolTipText("Digite sua data de nascimento");
		dataNascimentoTextField.setColumns(10);
		GridBagConstraints gbc_dataNascimentoTextField = new GridBagConstraints();
		gbc_dataNascimentoTextField.insets = new Insets(0, 0, 5, 0);
		gbc_dataNascimentoTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_dataNascimentoTextField.gridx = 0;
		gbc_dataNascimentoTextField.gridy = 3;
		panel.add(dataNascimentoTextField, gbc_dataNascimentoTextField);

		JLabel sexoLabel = new JLabel("Sexo");
		GridBagConstraints gbc_sexoLabel = new GridBagConstraints();
		gbc_sexoLabel.insets = new Insets(0, 0, 5, 0);
		gbc_sexoLabel.gridx = 0;
		gbc_sexoLabel.gridy = 4;
		panel.add(sexoLabel, gbc_sexoLabel);

		comboBox = new JComboBox<String>();
		sexoLabel.setLabelFor(comboBox);
		comboBox.setToolTipText("Selecione");
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] { "Masculino", "Feminino" }));
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 5;
		panel.add(comboBox, gbc_comboBox);

		JLabel alturaLabel = new JLabel("Altura");
		GridBagConstraints gbc_alturaLabel = new GridBagConstraints();
		gbc_alturaLabel.insets = new Insets(0, 0, 5, 0);
		gbc_alturaLabel.gridx = 0;
		gbc_alturaLabel.gridy = 6;
		panel.add(alturaLabel, gbc_alturaLabel);

		alturaTextField = new JTextField();
		alturaLabel.setLabelFor(alturaTextField);
		GridBagConstraints gbc_alturaTextField = new GridBagConstraints();
		gbc_alturaTextField.insets = new Insets(0, 0, 5, 0);
		gbc_alturaTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_alturaTextField.gridx = 0;
		gbc_alturaTextField.gridy = 7;
		panel.add(alturaTextField, gbc_alturaTextField);
		alturaTextField.setColumns(10);

		JLabel pesoLabel = new JLabel("Peso");
		GridBagConstraints gbc_pesoLabel = new GridBagConstraints();
		gbc_pesoLabel.insets = new Insets(0, 0, 5, 0);
		gbc_pesoLabel.gridx = 0;
		gbc_pesoLabel.gridy = 8;
		panel.add(pesoLabel, gbc_pesoLabel);

		pesoTextField = new JTextField();
		pesoLabel.setLabelFor(pesoTextField);
		GridBagConstraints gbc_pesoTextField = new GridBagConstraints();
		gbc_pesoTextField.insets = new Insets(0, 0, 5, 0);
		gbc_pesoTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_pesoTextField.gridx = 0;
		gbc_pesoTextField.gridy = 9;
		panel.add(pesoTextField, gbc_pesoTextField);
		pesoTextField.setColumns(10);

		MinhaListaOrdenavel listaOrdenavel = getMinhaListaOrdenavel();
		PessoaIMCTableModel imcTableModel = new PessoaIMCTableModel(listaOrdenavel);

		JButton cadastrarButton = new JButton("Cadastrar");
		ActionListener cadastroAction = new PostFormAction(nomeTextField, dataNascimentoTextField, comboBox,
				alturaTextField, pesoTextField, listaOrdenavel, imcTableModel);
		cadastrarButton.addActionListener(cadastroAction);
		GridBagConstraints gbc_cadastrarButton = new GridBagConstraints();
		gbc_cadastrarButton.insets = new Insets(0, 0, 5, 0);
		gbc_cadastrarButton.gridx = 0;
		gbc_cadastrarButton.gridy = 10;
		panel.add(cadastrarButton, gbc_cadastrarButton);

		JButton limparButton = new JButton("Limpar");
		ActionListener limparAction = new CleanFormAction(nomeTextField, dataNascimentoTextField, alturaTextField,
				pesoTextField);
		limparButton.addActionListener(limparAction);
		GridBagConstraints gbc_limparButton = new GridBagConstraints();
		gbc_limparButton.insets = new Insets(0, 0, 5, 0);
		gbc_limparButton.gridx = 0;
		gbc_limparButton.gridy = 11;
		panel.add(limparButton, gbc_limparButton);

		table = new JTable(imcTableModel);
		table.setAutoCreateRowSorter(true);
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.insets = new Insets(0, 0, 5, 0);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 12;
		panel.add(new JScrollPane(table), gbc_table);

	}

	public static MinhaListaOrdenavel getMinhaListaOrdenavel() {
		MinhaListaOrdenavel listaOrdenavel = new MinhaListaOrdenavel();

		listaOrdenavel.add(new Mulher("Maria", "12/01/1968", 54, 1.72));
		listaOrdenavel.add(new Mulher("Ana", "12/08/1998", 48, 1.65));
		listaOrdenavel.add(new Mulher("Mariane", "17/01/1990", 43, 1.63));
		listaOrdenavel.add(new Mulher("Isabella", "12/06/1987", 62, 1.78));
		listaOrdenavel.add(new Mulher("Larissa", "12/01/1984", 60, 1.73));
		listaOrdenavel.add(new Mulher("Z�lia", "14/05/1989", 62, 1.63));
		listaOrdenavel.add(new Homem("Carlos", "14/05/1989", 92, 1.85));
		listaOrdenavel.add(new Homem("Pedro", "14/05/1989", 98, 1.83));
		listaOrdenavel.add(new Homem("Samuel", "14/05/1989", 75, 1.75));
		listaOrdenavel.add(new Homem("Augusto", "14/05/1989", 80, 1.71));

		return listaOrdenavel;
	}
}
